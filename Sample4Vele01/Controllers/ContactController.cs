﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiPrototip.Models;
using WebApiPrototip.Services;

namespace WebApiPrototip.Controllers
{
    public class ContactController : ApiController
    {
        private ContactRepository contactRepository;

        public ContactController()
        {
            this.contactRepository = new ContactRepository();
        }

        public Contact Post(Contact contact)
        {
            Contact response = this.contactRepository.EvaluateContact(contact);

            return response;
        }
    }
}