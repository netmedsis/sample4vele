﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiPrototip.Models;


namespace WebApiPrototip.Services
{
    public class ContactRepository
    {
        public Contact EvaluateContact(Contact contact)
        {
            var ctx = HttpContext.Current;

            if (ctx != null)
            {
                try
                {
                    if (contact.Id % 2 == 0)
                    {
                        contact.JeParni = true;
                    }
                    else
                    {
                        contact.JeParni = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    contact.JeParni = false;
                }
            }
            else
            {
                contact.JeParni = false;    
            }

            return contact;
        }

    }
}